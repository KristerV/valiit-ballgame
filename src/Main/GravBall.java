package Main;

import javafx.scene.paint.Color;

public class GravBall extends Ball {

    public GravBall() {
        super(); // Käivita Ball constructor, kuna seal on handleClick()
        this.setRadius(30);
        this.setCenterX(200);
        this.setCenterY(200);
        this.setFill(Color.RED);
    }

    @Override
    protected void hide() {
        this.setOpacity(1 - (countBounce * 0.1));
    }

    @Override
    protected void move() {
        vectorY += 0.3;

        // X telg liikumine
        double praeguneX = this.getCenterX();
        double uusX = praeguneX + vectorX;
        this.setCenterX(uusX);

        // Y telg liikumine
        double praeguneY = this.getCenterY();
        double uusY = praeguneY + vectorY;
        this.setCenterY(uusY);

        this.setRadius(15 + Math.abs(vectorY));
    }
}
