package Main;

import javafx.scene.paint.Color;

public class BoxBall extends Ball {
    double vectorX = 0;
    double vectorY = 5;

    public BoxBall() {
        super();
        this.setRadius(30);
        this.setFill(Color.BLUE);
        this.setCenterX(100);
        this.setCenterY(100);
    }

    @Override
    protected void hide() {
        // Tee nii, et iga tick
        // jääb pall vähem näha näiteks
        // 0.01%. See on tehtav ühe ainsa
        // koodi reaga.
    }

    @Override
    protected void move() {

        double x = this.getCenterX();
        double y = this.getCenterY();
        if (x == 100 && y == 200) {
            vectorX = 5;
            vectorY = 0;
        } else if (x == 200 && y == 200) {
            vectorX = 0;
            vectorY = -5;
        } else if (x == 200 && y == 100) {
            vectorX = -5;
            vectorY = 0;
        } else if (x == 100 && y == 100) {
            vectorX = 0;
            vectorY = 5;
        }
        this.setCenterX(x + vectorX);
        this.setCenterY(y + vectorY);
    }

}
