package Main;

import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;

import java.util.List;

public abstract class Ball extends Circle implements Mover {
    double vectorX = Math.random() * 3;
    double vectorY = Math.random() * 3;
    int countBounce = 0;

    public Ball() {
        handleClick();
    }

    private void handleClick() {
        this.setOnMouseClicked(event -> {
            Pane pane = (Pane) this.getParent();
            List nodes = pane.getChildren();
            int index = nodes.indexOf(this);
            nodes.remove(index);
        });
    }

    // See meetod on kasutatav Game seest
    // näiteks ball.update()
    public void update() {
        move();
        bounce();
        hide();
    }

    protected abstract void hide();

    // Klassi sisene meetod, mis liigutab
    // palli x arv piksleid
    protected abstract void move();

    // Takistab ekraanist välja minemist,
    // nimelt pöörab vectori teistpidi.
    protected void bounce() {
        Pane pane = (Pane) this.getParent();
        double paneWidth = pane.getWidth()  - this.getRadius();
        double paneHeight = pane.getHeight()  - this.getRadius();

        // X telg
        double praeguneX = this.getCenterX();
        if (praeguneX > paneWidth || praeguneX < this.getRadius()) {
            vectorX *= -1;
        }

        // Y telg
        double praeguneY = this.getCenterY();
        if (praeguneY > paneHeight) {
            vectorY *= -1;
            this.setCenterY(paneHeight);
            countBounce++;
        } else if (praeguneY < this.getRadius()) {
            vectorY *= -1;
            this.setCenterY(this.getRadius());
        }

    }

}
