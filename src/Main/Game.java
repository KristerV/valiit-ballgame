package Main;

import javafx.animation.AnimationTimer;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

// https://gitlab.com/KristerV/valiit-ballgame

public class Game {
    int stageWidth;
    int stageHeight;
    Pane root = new Pane();
    Stage stage = new Stage();

    public Game(int width, int height) {
        stageWidth = width;
        stageHeight = height;
        createStage();
        createBalls();
        startGameLoop();
    }

    private void startGameLoop() {
        AnimationTimer loop = new AnimationTimer() {
            @Override
            public void handle(long l) {
                root.getChildren().forEach(node -> {
                    Ball ball = (Ball) node;
                    ball.update();
                });
                if (root.getChildren().size() == 0) {
                    Circle gg = new Circle(100, Color.RED);
                    StackPane sp = new StackPane();
                    sp.getChildren().add(gg);
                    stage.setScene(new Scene(sp, stageWidth, stageHeight));
                }
            }
        };
        loop.start();
    }

    private void createBalls() {
        BoxBall bball = new BoxBall();
        root.getChildren().add(bball);
        for (int i = 0; i < 10; i++) {
            FlyingBall ball = new FlyingBall();
            root.getChildren().add(ball);
            GravBall gball = new GravBall();
            root.getChildren().add(gball);
        }
    }

    private void createStage() {
        stage.setScene(new Scene(root, stageWidth, stageHeight));
        stage.show();
    }
}
